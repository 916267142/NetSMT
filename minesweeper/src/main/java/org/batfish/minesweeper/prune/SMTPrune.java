package org.batfish.minesweeper.prune;

import org.batfish.minesweeper.prune.SMTPruneUtil.*;
import org.batfish.minesweeper.prune.SMTPruneUtil.LinkType;

import java.util.HashMap;
import java.util.HashSet;

//
// Created by Charlie on 2024/3/18.
// XJTU | 916267142@qq.com
//

public class SMTPrune extends SMTPruneParser{
    ComponentPruneCore componentPruneCore;    // Prune algorithm of component
    DegreePruneCore degreePruneCore;          // Prune algorithm of degree
    NetworkGraph networkGraph;

    // The following structure represent the query answer, the link has three type of status
    // Type = 0 means free link,
    // Type = 1 means the link should be down,
    // Type = 2 means the link should be up.
    private HashMap<Link, LinkType> minesweeper_link_types;

    public SMTPrune() {
        componentPruneCore = new ComponentPruneCore();
        degreePruneCore = new DegreePruneCore();
        networkGraph = new NetworkGraph();
    }

    Link getLink(String src, String dst) {
        return this.networkGraph.graph.get(src).get(dst);
    }

    public void build() {
        this.links.forEach(this::addBidirectionalEdge);
        componentPruneCore.constructShrinkGraph();
    }

    void addBidirectionalEdge(Link link) {
        this.networkGraph.addBidirectionalEdge(link);
        this.componentPruneCore.addEdge(link.from_interface.device_name, link.to_interface.device_name);
        this.componentPruneCore.addEdge(link.to_interface.device_name, link.from_interface.device_name);
    }

    HashMap<String, HashMap<String, Link>> getGraph() {
        return this.networkGraph.graph;
    }

    public NetworkGraph getGraphByComponentPrune(String src, String dst) {
        NetworkGraph network_graph = new NetworkGraph();
        if (!this.networkGraph.containEdge(src, dst)) {
            return network_graph;
        }
        this.componentPruneCore.getRelevantLinks(src, dst).forEach(link -> {
            Link temp_link = getLink(link.src_name, link.dst_name);
            network_graph.addBidirectionalEdge(temp_link);
        });
        return network_graph;
    }

    public NetworkGraph getGraphByDegreePrune(NetworkGraph graph, String src, String dst) {
        if (!graph.containEdge(src, dst)) {
            return new NetworkGraph();
        }
        return this.degreePruneCore.getGraphByDegreePrune(graph, src, dst);
    }

    public void getMinesweeperConstraint(String src, String dst) {
        minesweeper_link_types = new HashMap<>();
        getGraph().forEach((key, value) -> value.forEach(
            (key1, link) -> minesweeper_link_types.put(link, LinkType.down_link)));

        // The first step is pruned by component
        NetworkGraph graph1 = this.getGraphByComponentPrune(src, dst);
        graph1.getGraph().forEach((key, value) -> value.forEach(
            (key1, link) -> minesweeper_link_types.put(link, LinkType.up_link)));

        // The second step is pruned by degree
        NetworkGraph graph2 = this.getGraphByDegreePrune(graph1, src, dst);
        graph2.getGraph().forEach((key, value) -> value.forEach(
            (key1, link) -> minesweeper_link_types.put(link, LinkType.free_link)));
    }

    public HashSet<SimpleLink> getMinesweeperConstraint(LinkType link_type) {
        HashSet<SimpleLink> links = new HashSet<>();
        minesweeper_link_types.forEach((link, type) -> {
            if (link_type.equals(type)) {
                links.add(new SimpleLink(link));
            }
        });
        return links;
    }

    public void displayMinesweeperConstraint(HashSet<LinkType> link_type) {
        minesweeper_link_types.forEach((link, type) -> {
            if (link_type.contains(type)) {
                System.out.println(link);
            }
        });
        System.out.println("the size of link is " + minesweeper_link_types.entrySet().stream()
            .filter(n -> link_type.contains(n.getValue())).count());
    }

    //The following is the test, and introduce the usage of SMTPrune.
    public static void main(String[] args) {
        // Step1. read topology and build
        SMTPrune smtPrune = new SMTPrune();
        smtPrune.readTopologyFromFile("/home/simple/Desktop/project/minesweeper-results/uscarrier/bgp/topology.txt");
        smtPrune.build();

        // Step2. calculate the constraint
        // smtPrune.getMinesweeperConstraint("sylva", "ellijay");
        smtPrune.getMinesweeperConstraint("panamattcity", "raleigh");

        // Step2. query the corresponding constraint
        HashSet<LinkType> type = new HashSet<>();
        type.add(LinkType.free_link);
        type.add(LinkType.free_link);
        smtPrune.displayMinesweeperConstraint(type);

    }
}
